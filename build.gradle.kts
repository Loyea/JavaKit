plugins {
    id("java-library")  //是为了dependencies中的'api'传递依赖给引用的项目，因为id=java的项目不能使用'api'
    id("maven-publish")
}

//对jitpack来说没用，jitpack的groupid固定就是com.github.loyea 这个是给maven中央仓库之类用的
group = "com.loyea.java"
//对jitpack来说并没有用，jitpack的版本号是用的github release的tag,或者选择commit进行构建时候的commit id
version = "23.4.8.0245"

repositories {
    mavenCentral()
}

dependencies {
    // https://mvnrepository.com/artifact/commons-io/commons-io
    api("commons-io:commons-io:+")

    // https://mvnrepository.com/artifact/commons-codec/commons-codec
    api("commons-codec:commons-codec:+")

    // 最后使用java的版本，4开始就是用kotlin了
    // https://mvnrepository.com/artifact/com.squareup.okhttp3/okhttp
    api("com.squareup.okhttp3:okhttp:3.14.9")

    // https://mvnrepository.com/artifact/com.squareup.okhttp3/logging-interceptor
    api("com.squareup.okhttp3:logging-interceptor:3.14.9")

    // https://mvnrepository.com/artifact/com.squareup.okhttp3/okhttp-dnsoverhttps
    api("com.squareup.okhttp3:okhttp-dnsoverhttps:3.14.9")

    // https://mvnrepository.com/artifact/org.apache.commons/commons-collections4
    api("org.apache.commons:commons-collections4:+")
    
    // https://mvnrepository.com/artifact/com.google.code.gson/gson
    api("com.google.code.gson:gson:+")

    // StringUtils.join(xxx, ",")
    // https://mvnrepository.com/artifact/org.apache.commons/commons-lang3
    api("org.apache.commons:commons-lang3:+")

    // https://mvnrepository.com/artifact/org.jsoup/jsoup
    api("org.jsoup:jsoup:+")

    // https://mvnrepository.com/artifact/org.json/json
    api("org.json:json:+")
    
    // https://mvnrepository.com/artifact/commons-net/commons-net
    api("commons-net:commons-net:+")

    // https://mvnrepository.com/artifact/org.apache.httpcomponents/httpclient
    api("org.apache.httpcomponents:httpclient:+")

    // https://mvnrepository.com/artifact/dnsjava/dnsjava
    api("dnsjava:dnsjava:+")

    // https://mvnrepository.com/artifact/org.apache.commons/commons-text
    api("org.apache.commons:commons-text:+")

    // https://mvnrepository.com/artifact/net.sourceforge.htmlcleaner/htmlcleaner
    api("net.sourceforge.htmlcleaner:htmlcleaner:+")

    // https://mvnrepository.com/artifact/org.zeroturnaround/zt-zip
    api("org.zeroturnaround:zt-zip:+")

    // mysql 8.0+ jdbc驱动
    // https://mvnrepository.com/artifact/com.mysql/mysql-connector-j
    api("com.mysql:mysql-connector-j:+")

    // sqlite JDBC驱动
    // https://mvnrepository.com/artifact/org.xerial/sqlite-jdbc
    api("org.xerial:sqlite-jdbc:+")
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            from(components["java"])
        }
    }
}