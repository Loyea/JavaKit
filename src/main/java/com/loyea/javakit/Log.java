package com.loyea.javakit;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Log {
    private static final SimpleDateFormat simpleDateFormat;

    static {
        simpleDateFormat = new SimpleDateFormat("M/d HH:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+8"));
    }

    public static final int LEVEL_DEBUG = 0;
    public static final int LEVEL_INFO = 1;
    public static final int LEVEL_ERROR = 2;

    static String[] levelAbbrs = {"D", "I", "E"};

    static int outputLevel = LEVEL_DEBUG;

    public static void show(String log) {
        info(log);
    }

    private static void show(int level, String log, Throwable e) {
        if (level >= outputLevel) {
            String levelAbbr = levelAbbrs[level];
            print(levelAbbr, log);
            if (e != null) {
                print(levelAbbr, "==============");
                e.printStackTrace();
                print(levelAbbr, "==============");
            }
        }
    }

    private static void print(String levelAbbr, String log) {
        System.out.println(String.format("%s-%s-%s: %s",
                levelAbbr,
                Thread.currentThread().getName(),
                simpleDateFormat.format(new Date()),
                log));
    }

    public static void debug(String debug) {
        show(LEVEL_DEBUG, debug, null);
    }

    public static void info(String info) {
        show(LEVEL_INFO, info, null);
    }

    public static void error(String error) {
        error(error, null);
    }

    public static void error(String error, Throwable throwable) {
        show(LEVEL_ERROR, error, throwable);
    }

    public static void setOutputLevel(String level) {
        switch (level) {
            case "info":
                outputLevel = LEVEL_INFO;
                break;
            case "error":
                outputLevel = LEVEL_ERROR;
                break;
        }
    }

    public static void setOutputLevel(int level) {
        outputLevel = level;
    }
}
